#!/usr/bin/env python
#
#  Licensed to the Apache Software Foundation (ASF) under one
#  or more contributor license agreements.  See the NOTICE file
#  distributed with this work for additional information
#  regarding copyright ownership.  The ASF licenses this file
#  to you under the Apache License, Version 2.0 (the
#  "License"); you may not use this file except in compliance
#  with the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing,
#  software distributed under the License is distributed on an
#  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
#  KIND, either express or implied.  See the License for the
#  specific language governing permissions and limitations
#  under the License.
#
'''The match module for Mouse.'''

import guesser


class UnknownResult(object):

  def __init__(self):
    self.header_name = '?????'
    self.license_family = '?????'
    self.license_approved = False
    self.type_name = 'standard'
    self.include_sample = True


class NoticeResult(object):

  def __init__(self):
    self.type_name = 'notice'


class ArchiveResult(object):

  def __init__(self):
    self.type_name = 'archive'


class BinaryResult(object):

  def __init__(self):
    self.type_name = 'binary'


_match_order = [
    ( guesser.is_notice, NoticeResult ),
    ( guesser.is_archive, ArchiveResult ),
    ( guesser.is_binary, BinaryResult ),

    # we don't need UnknownResult in this list, since it will be returned
    # from do_match() by default.
    # ( lambda x: True, UnknownResult ),
  ]


def do_match(item):
  '''Check each of the classes in _match_order, using it's match() method, and
     if the content of item matches that class, instantiate and return the
     result.'''
  for (matcher, result) in _match_order:
    if matcher(item):
      return result()

  return UnknownResult()
