#!/usr/bin/env python
#
#  Licensed to the Apache Software Foundation (ASF) under one
#  or more contributor license agreements.  See the NOTICE file
#  distributed with this work for additional information
#  regarding copyright ownership.  The ASF licenses this file
#  to you under the Apache License, Version 2.0 (the
#  "License"); you may not use this file except in compliance
#  with the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing,
#  software distributed under the License is distributed on an
#  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
#  KIND, either express or implied.  See the License for the
#  specific language governing permissions and limitations
#  under the License.
#
'''Module to determine if a file with given content is an archive.'''

import os

_archive_exts = [ 'jar', 'gz', 'zip', 'tar', 'bz', 'bz2', 'rar', 'war',
  ]


def is_archive(item):
  '''Entry method, will return True if ITEM is thought to be a notice,
     False otherwise.'''

  (dirname, basename) = os.path.split(item.name)

  # Look at the extensions
  for ext in _archive_exts:
    if basename.endswith('.' + ext):
      return True

  ### TODO: We could actually look at the file contents, see
  ### zipfile.is_zipfile() and tarfile.is_tarfile().

  # we've exhausted our options, so this file must not be an archive
  return False
