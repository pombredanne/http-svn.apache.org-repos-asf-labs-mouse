#!/usr/bin/env python
#
#  Licensed to the Apache Software Foundation (ASF) under one
#  or more contributor license agreements.  See the NOTICE file
#  distributed with this work for additional information
#  regarding copyright ownership.  The ASF licenses this file
#  to you under the Apache License, Version 2.0 (the
#  "License"); you may not use this file except in compliance
#  with the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing,
#  software distributed under the License is distributed on an
#  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
#  KIND, either express or implied.  See the License for the
#  specific language governing permissions and limitations
#  under the License.
#
'''Module to determine if a file with given content is a notice.'''

import os

_notice_names = [ 'NOTICE', 'LICENSE', 'LICENSE.TXT', 'NOTICE.TXT', 'INSTALL',
    'INSTALL.TXT', 'README', 'README.TXT', 'NEWS', 'NEWS.TXT', 'AUTHOR',
    'AUTHOR.TXT', 'AUTHORS', 'AUTHORS.txt', 'CHANGELOG', 'CHANGELOG.TXT',
    'DISCLAIMER', 'DISCLAIMER.TXT', 'KEYS', 'KEYS.TXT', 'RELEASE-NOTES',
    'RELEASE-NOTES.TXT', 'RELEASE_NOTES', 'RELEASE_NOTES.TXT', 'UPGRADE',
    'UPGRADE.TXT', 'STATUS', 'STATUS.TXT', 'THIRD_PARTY_NOTICES',
    'THIRD_PARTY_NOTICES.TXT', 'COPYRIGHT', 'COPYRIGHT.TXT', 'BUILDING',
    'BUILDING.TXT', 'BUILD', 'BUILT.TXT',
  ]

_notice_exts = [ 'LICENSE', 'LICENSE.TXT', 'NOTICE', 'NOTICE.TXT',
  ]


def is_notice(item):
  '''Entry method, will return True if ITEM is thought to be a notice,
     False otherwise.'''

  # Just look a the filename
  (dirname, basename) = os.path.split(item.name)
  if basename.upper() in _notice_names:
    return True

  # now look at the extensions
  for ext in _notice_exts:
    if basename.endswith('.' + ext):
      return True

  # we've exhausted our options, so this file must not be a notice
  return False
