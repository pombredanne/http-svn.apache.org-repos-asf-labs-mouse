#!/usr/bin/env python
#
#  Licensed to the Apache Software Foundation (ASF) under one
#  or more contributor license agreements.  See the NOTICE file
#  distributed with this work for additional information
#  regarding copyright ownership.  The ASF licenses this file
#  to you under the Apache License, Version 2.0 (the
#  "License"); you may not use this file except in compliance
#  with the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing,
#  software distributed under the License is distributed on an
#  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
#  KIND, either express or implied.  See the License for the
#  specific language governing permissions and limitations
#  under the License.
#
'''Module to determine if a file with given content is binary.'''

import os
import mimetypes


_data_exts = [
    'dat', 'doc', 'ncb', 'idb', 'suo', 'xcf', 'raj', 'cert', 'ks', 'ts', 'odp',
  ]

_exec_exts = [
    'exe', 'dll', 'lib', 'so', 'a', 'exp',
  ]

_keystore_exts = [
    'jks', 'keystore', 'pem', 'crl',
  ]

_image_exts = [
    'png', 'pdf', 'gif', 'giff', 'tif', 'tiff', 'jpg', 'jpeg', 'ico', 'icns',
  ]

_bytecode_exts = [
    'class', 'pyd', 'obj', 'pyc',
  ]

_binary_exts = _data_exts + _exec_exts + _keystore_exts + _image_exts + \
               _bytecode_exts

_non_ascii_threshold = 256;
_ascii_char_threshold = 8;
_high_bytes_ratio = 100;
_total_read_ratio = 30;


def is_binary(item):
  '''Entry method, will return True if ITEM is thought to be binary,
     False otherwise.'''

  # First, try the mime-type
  (type, encoding) = mimetypes.guess_type(item.name)
  if type and type.split('/')[0] in ('image', 'application'):
    return True

  # Now, manually look at file extensions
  (root, ext) = os.path.splitext(item.name)
  if ext[1:].lower() in _binary_exts:
    return True

  # Special case: we still think is is binary if it contains an executable
  # extension in the filename, not just at the end
  for ext in _exec_exts:
    if ('.' + ext + '.') in item.name.lower():
      return True

  # Time to attempt a brute-force divination
  high_bytes = 0
  content = item.get_content().read(100)
  for c in content:
    if ord(c) > _non_ascii_threshold or ord(c) <= _ascii_char_threshold:
      high_bytes += 1

  if (high_bytes * _high_bytes_ratio) > \
            (min(100, len(content)) * _total_read_ratio):
    return True

  # we've exhausted our options, so this file must not be binary
  return False
