#!/usr/bin/env python
#
#  Licensed to the Apache Software Foundation (ASF) under one
#  or more contributor license agreements.  See the NOTICE file
#  distributed with this work for additional information
#  regarding copyright ownership.  The ASF licenses this file
#  to you under the Apache License, Version 2.0 (the
#  "License"); you may not use this file except in compliance
#  with the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing,
#  software distributed under the License is distributed on an
#  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
#  KIND, either express or implied.  See the License for the
#  specific language governing permissions and limitations
#  under the License.
#
'''A module to help Mouse guess the different kinds of files.

Helper methods are defined here so that consumers only need import this
module, rather than all the child modules.'''

import binary
import notice
import archive

def is_binary(item):
  return binary.is_binary(item)

def is_notice(item):
  return notice.is_notice(item)

def is_archive(item):
  return archive.is_archive(item)
